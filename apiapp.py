import json
import math
from flask import Flask
from flask import request
from transformers import pipeline
from itertools import combinations
from cleantext.sklearn import CleanTransformer
from sentence_transformers import SentenceTransformer, util


def cstr(el):
    if len(el) > 1:
        s = ' '.join(el)
    else:
        s = el[0]
    return s


def sort_dict(d,reverse=True):
    return dict(sorted(d.items(), key=lambda item: item[1], reverse=reverse))


def calc_santiments(bio):
    """
    LABEL_0 - negative    bios_scores * (1-sentiments_scores)/3;
    LABEL_1 - neutral     bios_scores * sentiments_scores/2
    LABEL_2 - positive    bios_scores * sentiments_scores if sentiments_scores > 0.5
    """
    sentiment_analysis = pipeline("sentiment-analysis", model="cardiffnlp/twitter-roberta-base-sentiment")
    item = sentiment_analysis(bio)
    if item[0]['label'] == 'LABEL_0':
        return float((1-item[0]['score'])/3)
    if item[0]['label'] == 'LABEL_1':
        return float(item[0]['score']/2)
    if item[0]['label'] == 'LABEL_2':
        if item[0]['score'] > 0.5:
            return item[0]['score']
        else:
            return 0.5


app = Flask(__name__)


@app.route("/")
def placeholder():
    return "Replayed.co API."


# allow POST requests
@app.route("/staff-bios", methods=['POST'])
def staff_bios():
    if request.method == 'POST':
        request_json = request.get_json()
        if 'video_tags' in request_json and 'editors' in request_json:
            video_tags = [request_json['video_tags']]
            editors = request_json['editors']
            # Load the models
            model = SentenceTransformer('all-mpnet-base-v2')
            results = {}
            bios_results = {}
            # Prepare video tags
            cleaner = CleanTransformer(fix_unicode=True, no_emails=True, no_urls=True, no_punct=True, lower=True)
            video_tags_clean = cleaner.transform(video_tags)
            video_embeddings = model.encode(video_tags_clean, convert_to_tensor=True)
            for k, v in editors.items():
                skills = v[0].split(', ')
                cmb = [list(combinations(skills, i + 1)) for i in range(len(skills))]
                sclist = []
                for item in cmb:
                    mobj = map(cstr, item)
                    sclist.extend(list(mobj))
                skills_embeddings = model.encode(sclist, convert_to_tensor=True)
                bios_embeddings = model.encode(v[1], convert_to_tensor=True)
                cosine_scores = util.cos_sim(video_embeddings, skills_embeddings)
                cosine_bios_scores = util.cos_sim(video_embeddings, bios_embeddings)
                # How is positive his bio?
                multiplier = calc_santiments(v[1])
                best_scores = float(max(cosine_scores[0]))
                results[k] = math.floor(best_scores * 100)
                bios_results[k] = math.floor(float(cosine_bios_scores[0]) * 100 * multiplier)
            results = sort_dict(results)
            bios_results = sort_dict(bios_results)
            total = {}
            for k, v in results.items():
                total[k] = v + bios_results[k]
            total = sort_dict(total)
            return json.dumps(total, indent=4)
        else:
            return "No input data!"


if __name__ == "__main__":
    app.run()
