## Endpoints
1. **/staff-bios** in file **/API/apiapp.py** method **staff_bios** accepts POST requests  
## Request example  
```
POST /staff-bios HTTP/1.1  
Host: ****
Content-Type: application/json  
Content-Length: 1278  

{
    "video_tags": "1000 pokemon packs, 2020 elections, buying a car, buying a new subaru, buying a subaru, buying a used car, buying a used subaru, cornwell tools, democratic party, gpu shortage, how to sell on amazon 2019, lake powell, mississippi department of education, party, partypoker, paulshardware, pokemon cards, pokerstars, porcelain, roll cart, scuba diving lake powell, shannon sailboats for sale, shellback tech, should i buy a subaru, unbroken bonds 1000 packs",
    "editors": {
        "John": [
            "Graphics, CGI, Meme, TV style, Commentary, Film, Satire, Musician, Documentary",
            "Producer & Graphics Specialist, Eugene is a producer at Replayed, previously working in TV and documentary editing."
        ],
        "Bob": [
            "Graphics, Meme, Reality, Vlog, Gaming, Comedy, Sports",
            "Producer & Senior Editor, Working with the UK creator scene, rising from humble beginnings, Jamie knows his way around the block."
        ],
        "Rob": [
            "Podcast, Comedy, TV style, vlog, Education, Science, Film",
            "Producer & Exec Editor, From making BAFTA accredited films, to British comedy series and Netflix story editing, whatever you throw at Joe, he can work his magic."
        ]
    }
}
```
---
